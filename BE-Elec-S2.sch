EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 5236 4016
encoding utf-8
Sheet 1 3
Title "BE d'Elec - Semestre 2"
Date ""
Rev ""
Comp "Grenoble-Inp Phelma"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1000 1000 1450 1000
U 61D818BB
F0 "Partie-1" 50
F1 "Partie-1.sch" 50
$EndSheet
$Sheet
S 2550 1000 1400 1000
U 61D81993
F0 "Partie-2" 50
F1 "Partie-2.sch" 50
$EndSheet
$EndSCHEMATC
