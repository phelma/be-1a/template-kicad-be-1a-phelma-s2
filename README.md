# Template Kicad BE 1A Phelma - S2

Template pour les BE d'Elec du semestre 2

Check out the repository and the submodule:
```bash
$ git clone --recurse-submodules git@gricad-gitlab.univ-grenoble-alpes.fr:phelma/be-1a/template-kicad-be-1a-phelma-s2.git
```
